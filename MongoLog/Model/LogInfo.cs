﻿using System.Collections.Generic;

namespace MongoLog
{
    public class LogInfo
    {
        /// <summary>
        /// 模式
        /// </summary>
        public string Folder { get; set; }

        /// <summary>
        /// Log種類
        /// </summary>
        public string SubFolder { get; set; }

        /// <summary>
        /// Log種類
        /// </summary>
        public LogMode LogMode { get; set; }

        /// <summary>
        /// 寫入內容
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object[] Args { get; set; }

        /// <summary>
        /// 是否寫入DB預設
        /// </summary>
        public bool IsWriteToMongo { get; set; } = true;

        /// <summary>
        /// 庫名
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string CollectionName { get; set; }

        /// <summary>
        /// 存放mongo欄位
        /// </summary>
        public Dictionary<string, object> dictionary { get; set; }
    }
}
