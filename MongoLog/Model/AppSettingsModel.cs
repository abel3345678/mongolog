﻿namespace MongoLog
{
    public class AppSettingsModel
    {
        /// <summary>
        /// 連線字串
        /// </summary>
        public Connectionstring ConnectionString { get; set; }

        public class Connectionstring
        {
            /// <summary>
            /// mongo連線字串
            /// </summary>
            public string mongodb { get; set; }
        }

        /// <summary>
        /// 站別
        /// </summary>
        public string Station { get; set; }

    }
}
