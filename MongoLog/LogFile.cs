﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using NLog;
using NLog.Config;
using NLog.Mongo;
using System;
using System.Timers;

namespace MongoLog
{
    public enum LogMode
    {
        Error,           //錯誤訊息
        Down,            //下載數據
        DownInfo,        //解析後的數據
        Update,          //執行過的Sql
        AllianceTeam,    //新建聯盟隊伍紀錄
        BigBall_send,    //發送大球紀錄
        BigBall_resp,    //接收大球紀錄
        Illegal,         //比分核對紀錄
        Callback,        //送分送回來的分數
        Runs,            //記錄比分
        RequestScore,    //送出request記錄
        UpdateScoreDB,   //紀錄送分更新資料庫
        WSMessage,       //紀錄所有 WebSocket 引發 OnMessage 事件所接收到的指令數據
        WSInfo,          //紀錄 A隊 vs B隊 跟盤程式成功執行的指令數據
        StopAndContinue, //斷跟紀錄
        AnalysisDown,    //(足球)解析下載
        IP,              //WS回傳的連接IP
        Tips,            //提示訊息
        BackScore,       //比分倒退
        scPrompt,        //足球入球提示
        RealGameTips,    //多來源提示
        WebIDMatch,      //觀察WEBID正不正常
        UnknownAllianceTeam  //聯盟隊伍新增紀錄
    }

    public class LogFile
    {
        private static int TimerInterval = 10 * 1000; //計時器間隔時間 單位(毫秒)
        private static int ConectTimeout = 1; //資料庫連線超時秒數
        private static string Station = Config.ReadFromAppSettings().Get<AppSettingsModel>().Station; //站別
        private static string ConnectionString = Config.ReadFromAppSettings().Get<AppSettingsModel>().ConnectionString.mongodb; //連線字串
        private static Timer Timer = new Timer(); //計時器(測試DB連線)
        private static Logger Logger; //Logger類別
        private static WriteMode WirteMode = WriteMode.Mongo; //目前寫入模式
        private enum WriteMode { Mongo, Local }; //寫入模式

        /// <summary>
        /// 測試連線
        /// </summary>
        /// <returns></returns>
        private static void ConnectTest(Object source, ElapsedEventArgs e)
        {
            bool result = true;
            Timer.Interval = TimerInterval;
            TimeSpan timout = TimeSpan.FromSeconds(ConectTimeout);
            MongoClient client = new MongoClient(ConnectionString);
            MongoServer server = client.GetServer();

            try
            {
                server.Connect(timout);
                server.Disconnect();
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                Timer.Enabled = !result;
                WirteMode = result ? WriteMode.Mongo : WriteMode.Local;
            }
        }

        /// <summary>
        /// 寫入日誌 (DB)
        /// </summary>
        /// <param name="logInfo"></param>
        private static void Mongo(LogInfo loginfo)
        {
            LogEventInfo logEventInfo = new LogEventInfo();
            logEventInfo.TimeStamp = DateTime.Now;
            logEventInfo.Level = LogLevel.Info;
            logEventInfo.Properties["LogName"] = loginfo.LogMode;

            try
            {
                // 更改target header
                LoggingConfiguration logConfig = LogManager.Configuration;
                MongoTarget tatget = (MongoTarget)logConfig.FindTargetByName("Mongo"); //要修改Target的Name       
                tatget.DatabaseName = loginfo.DatabaseName; //修改database名稱
                tatget.CollectionName = loginfo.CollectionName; //修改collection名稱
                LogManager.ReconfigExistingLoggers(); //儲存修改的config

                // 加入屬性
                foreach (var item in loginfo.dictionary)
                {
                    logEventInfo.Properties[item.Key] = item.Value;
                }

                Logger = LogManager.GetLogger("Mongo");
                Logger.Log(logEventInfo);
            }
            catch (Exception)
            {
                // mongo斷線
                WirteMode = WriteMode.Local;
                Timer.Enabled = true;
                // 紀錄本地日誌
                SetLog(loginfo);
            }
        }

        /// <summary>
        /// 寫入日誌 (本地)
        /// </summary>
        /// <param name="logInfo"></param>
        private static void Local(LogInfo logInfo)
        {
            LogEventInfo logEventInfo = new LogEventInfo();
            logEventInfo.TimeStamp = DateTime.Now;
            logEventInfo.Level = LogLevel.Info;
            logEventInfo.Message = Station + logInfo.Text;
            logEventInfo.Properties["Sport"] = logInfo.Folder;

            if (logInfo.LogMode != LogMode.Error && logInfo.LogMode != LogMode.Callback &&
                logInfo.LogMode != LogMode.UpdateScoreDB && logInfo.LogMode != LogMode.RequestScore &&
                logInfo.LogMode != LogMode.Tips && logInfo.LogMode != LogMode.scPrompt)
            {
                //針對送分比分作紀錄
                if (logInfo.LogMode == LogMode.Runs)
                {
                    Logger = LogManager.GetLogger("RunsLog");
                    logEventInfo.Properties["RunsLog"] = logInfo.LogMode.ToString();
                    logEventInfo.Properties["GID"] = logInfo.Text;
                    logEventInfo.Properties["LogName"] = logInfo.LogMode.ToString() + string.Format("_{0}", DateTime.Now.Hour.ToString());
                    logEventInfo.Message = logInfo.Args[0].ToString();
                }
                else if (LogMode.WSInfo == logInfo.LogMode || (LogMode.DownInfo == logInfo.LogMode && logInfo.Args.Length > 0) || LogMode.BackScore == logInfo.LogMode)
                {
                    Logger = LogManager.GetLogger("FollowHour");    //以小時為Log紀錄區分
                    logEventInfo.Properties["FolderName"] = logInfo.LogMode.ToString();
                    logEventInfo.Properties["LogName"] = logInfo.Args[0];
                }
                else
                {
                    Logger = LogManager.GetLogger("FollowHour");    //以小時為Log紀錄區分
                    logEventInfo.Properties["FolderName"] = logInfo.LogMode.ToString();
                    logEventInfo.Properties["LogName"] = logInfo.LogMode.ToString() + string.Format("_{0}", DateTime.Now.Hour.ToString());
                }
            }
            else
            {
                Logger = LogManager.GetLogger("FollowDay");      //以日期為Log紀錄區分
                logEventInfo.Properties["LogName"] = logInfo.LogMode.ToString();
                logEventInfo.Level = (logInfo.LogMode != LogMode.Error) ? LogLevel.Info : LogLevel.Error;
            }

            Logger.Log(logEventInfo);
        }

        /// <summary>
        /// 寫入日誌 (本地)
        /// </summary>
        /// <param name="logInfo"></param>
        public static void Local2(LogInfo logInfo)
        {
            LogEventInfo logEventInfo = new LogEventInfo();
            logEventInfo.TimeStamp = DateTime.Now;
            logEventInfo.Level = LogLevel.Info;
            logEventInfo.Message = Station + logInfo.Text;
            logEventInfo.Properties["Sport"] = logInfo.Folder;

            Logger = LogManager.GetLogger("FollowHour");    //以小時為Log紀錄區分
            logEventInfo.Properties["FolderName"] = logInfo.SubFolder;
            logEventInfo.Properties["LogName"] = logInfo.SubFolder + string.Format("_{0}", DateTime.Now.Hour.ToString());
            Logger.Log(logEventInfo);
        }

        /// <summary>
        /// 寫入日誌 (選擇何種寫入模式)
        /// </summary>
        /// <param name="logInfo">LogFile.SetLog參數</param>
        public static void SetLog(LogInfo loginfo)
        {
            switch (WirteMode)
            {
                case WriteMode.Mongo:
                    // 有些日誌只有本地才需要
                    if (loginfo.IsWriteToMongo)
                        Mongo(loginfo);
                    break;
                case WriteMode.Local:
                    if (string.IsNullOrEmpty(loginfo.SubFolder))
                        Local(loginfo);
                    else
                        Local2(loginfo);
                    break;
            }
        }

        /// <summary>
        /// 計時器初始化
        /// </summary>
        public static void TimerInit()
        {
            Timer.Elapsed += ConnectTest;
            Timer.Enabled = true;
            Timer.AutoReset = true;
        }

        /// <summary>
        /// 將NLog Buffer中的尚未輸出的日誌紀錄輸出
        /// </summary>
        public static void Flush()
        {
            LogManager.Flush(e => { }, 1000);
        }
    }
}
