﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace MongoLog
{
    public class Config
    {
        /// <summary>
        /// 設定源目錄
        /// </summary>
        public static void SetCurrentDirectory()
        {
            int index = AppContext.BaseDirectory.IndexOf("bin");
            string basePath = index == -1 ? Directory.GetCurrentDirectory() : AppContext.BaseDirectory.Substring(0, index);
            Directory.SetCurrentDirectory(basePath);
        }

        /// <summary>
        /// 取得組態設定的方法
        /// </summary>
        /// <returns></returns>
        public static IConfigurationRoot ReadFromAppSettings()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }
    }
}
