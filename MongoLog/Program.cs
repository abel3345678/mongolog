﻿using System;
using System.Collections.Generic;

namespace MongoLog
{
    class Program
    {
        static void Main(string[] args)
        {
            Config.SetCurrentDirectory();
            LogFile.TimerInit();

            string text = "123";
            string content = "SA÷5¬~FG÷-2:1563130800;2:1563463800¬~SG÷47_MZFZnvX4;200_rJVAIaHo¬~~A1÷f4555b83cda7c25f150b616ebab3d953¬~";

            LogInfo logInfo = new LogInfo()
            {
                Folder = "TNFS",
                LogMode = LogMode.Down,
                Text = text,
                DatabaseName = "Follow_TNFS",
                CollectionName = "TNFS",
                dictionary = new Dictionary<string, object>()
                {

                    ["Msg_proxy"] = "代理名稱",
                    ["Msg_type"] = 1,
                    ["Msg_url"] = "http://d.flashscore.com/x/feed/f_5_-1_8_en_1",
                    ["Msg_content"] = content
                }

            };

            LogFile.SetLog(logInfo);

            Console.ReadLine();
        }
    }
}
